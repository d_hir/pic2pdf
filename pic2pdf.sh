#!/usr/bin/env bash

if [ "$1" = "" ]
then
  PICFOLDER=.
  FILENAME=pic2pdf
else
  if [ -d $1 ]
  then
    PICFOLDER=$1
    FILENAME=$(basename $PICFOLDER)
  else
    echo "Given argument is not a directory!"
    exit
  fi
fi

OIFS="$IFS"
IFS=$'\n'

TEXFILE=$FILENAME.tex
LOGFILE=$FILENAME_compilation.log

echo "\documentclass{article}
\usepackage[paperheight=10.8in,paperwidth=19.2in,margin=0in]{geometry}
\usepackage{graphicx}
\begin{document}" > $TEXFILE

for file in `ls $PICFOLDER | grep .png`
do
  echo "Found $file"
  echo "\noindent\includegraphics[width=.9999\paperwidth]{$PICFOLDER/$file}" >> $TEXFILE
  echo >> $TEXFILE
done

echo "\end{document}" >> $TEXFILE

echo "Done writing TEX file"
echo "Running pdflatex..."

pdflatex $TEXFILE > $LOGFILE

echo "Remove temporary files..."
rm $FILENAME.aux
rm $FILENAME.log
rm $TEXFILE
rm $LOGFILE

IFS="$OIFS"
